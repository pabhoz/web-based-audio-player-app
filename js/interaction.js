$(function(){

	var contenedor = $("body");

	var deviceW = window.screen.width;
	var deviceH = window.screen.height;

	console.log(deviceW);

	var songs = [
		{
			title: "Fuel",
			url: "./songs/1.mp3",
			img: "./covers/1.jpg",
			album: "Reload"
		},
		{
			title: "Big Bad Wolf",
			url: "./songs/2.mp3",
			img: "./covers/2.jpeg",
			album: "Black Widow"
		},
		{
			title: "Work",
			url: "./songs/3.mp3",
			img: "./covers/3.png",
			album: "Anti"
		}
	];

	var player = $("audio")[0];

	var touches = {
		initial: {
			x: null,
			y: null
		},
		last: {
			x: null,
			y: null
		}
		
	}

	console.log("Asignando listeners");

	contenedor.on("touchstart",function(e){
		var e = e.originalEvent;
		var touchObj = e.changedTouches[0];

		touches.initial.x = parseInt(touchObj.clientX);
		touches.initial.y = parseInt(touchObj.clientY);

		console.log("Inicio del toque");
	})
	.on("touchmove",function(e){
		var e = e.originalEvent;
		var touchObj = e.changedTouches[0];

		//console.log(touches);

		//console.log("Me muevo!");
	})
	.on("touchend",function(e){
		var e = e.originalEvent;
		var touchObj = e.changedTouches[0];

		touches.last.x = parseInt(touchObj.clientX);
		touches.last.y = parseInt(touchObj.clientY);

		console.log("Fin del toque");

		console.log("validando gestos");
		evaluateGestures();
	});

	function evaluateGestures(){

		var cambioX = touches.initial.x - touches.last.x;
		var cambioY = touches.initial.y - touches.last.y;

		console.log(cambioX);

		if(cambioX == 0 && cambioY == 0){
			playSong();
		}

		console.log("Cambio absoluto: "+Math.abs(cambioX));
		if(Math.abs(cambioX) > 0.8 * deviceW){
			if(cambioX < 0){
				prevSong();
			}else{
				nextSong();
			}
		}

		if(cambioY > 0){
			
		}else{
			
		}

	}

	function nextSong(){
		if(!isTheLastSong(getActualSong()))
		{
			var index = getActualSong() + 1;
			var song = songs[index];
			player.src = song.url;
			player.play();
			player.dataset.index = index;
			changeSongInfo(song);
		}
	}

	function prevSong(){
		if(!isTheFirstSong(getActualSong()))
		{
			var index = getActualSong() - 1;
			var song = songs[index];
			player.src = song.url;
			player.play();
			player.dataset.index = index;
			changeSongInfo(song);
		}
	}

	function playSong(){
		if(player.paused){
					player.play();
		}else{
					player.pause();
		}
	}

	function getActualSong(){
		return parseInt(player.dataset.index);
	}

	function isTheLastSong(index){
		var itIs = false;

		if(songs.length == (index + 1)){
			itIs = true;
		}

		return itIs;
	}

	function isTheFirstSong(index){
		var itIs = false;

		if(index == 0){
			itIs = true;
		}

		return itIs;
	}

	function changeSongInfo(song){
		$("#title").html(song.title);
		$("#caratula img")[0].src = song.img;
	}

});